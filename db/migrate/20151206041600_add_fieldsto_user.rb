class AddFieldstoUser < ActiveRecord::Migration
  def change
    add_column :users, :zipcode, :string
    add_column :users, :name, :string
  end
end
