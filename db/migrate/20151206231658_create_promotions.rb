class CreatePromotions < ActiveRecord::Migration
  def change
    create_table :promotions do |t|
      t.references :user, index: true, foreign_key: true
      t.boolean :applied, default: false
      t.decimal :amount, scale: 2, precision: 8, default: 0

      t.timestamps null: false
    end

    add_index :promotions, [:user_id, :applied]
  end
end
