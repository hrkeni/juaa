class AddMoneyToUser < ActiveRecord::Migration
  def change
    add_column :users, :money, :decimal, scale: 2, precision: 8, default: 0
  end
end
