require 'rails_helper'

RSpec.describe User, type: :model do
  describe 'Validations' do
    it "is invalid without an email" do
      u = User.new email: nil
      expect(u.valid?).to be false
    end

    it "is invalid if email is not unique" do
      u = User.create email: "harshith.keni@gmail.com"
      # expect(u.valid?).to be true
      u = User.create email: "harshith.keni@gmail.com"
      expect(u.valid?).to be false
    end

    it "is invalid if it is not a valid email address" do
      u = User.new email: 'invalid email'
      expect(u.valid?).to be false
    end

    it "is invalid if there is no password" do
      u = User.new email: 'harshith.keni@gmail.com', password: nil
      expect(u.valid?).to be false
    end

    it "is invalid if password is too short" do
      u = User.new email: 'harshith.keni@gmail.com', password: 'abc12'
      expect(u.valid?).to be false
    end

    it "is invalid if password is too long" do
      u = User.new email: 'harshith.keni@gmail.com',
                    password: 'abc12abc12abc12abc123'
      expect(u.valid?).to be false
    end

    it "is invalid if zipcode is invalid" do
      u = User.new zipcode: '213dff'
      expect(u.valid?).to be false
    end

    it "is invalid if there is no name" do
      u = User.new name: ''
      expect(u.valid?).to be false
    end

    it "is valid" do
      u = User.create email: 'harshith.keni@gmail.com',
                    password: 'abc123',
                    zipcode: '57006',
                    name: 'Harshith Keni'
      expect(u.valid?).to be true
    end

    it "is not valid if money is negative" do
      u = User.new email: 'harshith.keni@gmail.com',
                    password: 'abc123',
                    zipcode: '57006',
                    name: 'Harshith Keni',
                    money: -1
      expect(u.valid?).to be false
    end
  end
end
