class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :confirmable

  validates :email, presence: true, uniqueness: true,
                    format: Devise.email_regexp
  validates :password, presence: true, confirmation: true, length: {in: 6..20}
  validates :zipcode, presence: true, zipcode: {country_code: :us}
  validates :name, presence: true
  validates :money, numericality: {greater_than_or_equal_to: 0}

  has_many :promotions

  after_create do |user|
    Promotion.create(user: user, amount: 20)
  end

  def promotions_pending?
    Promotion.where(user: self, applied: false).count > 0
  end
end
