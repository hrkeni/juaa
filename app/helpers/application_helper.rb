module ApplicationHelper
  def bootstrap_class_for flash_type
    { success: "alert-success", error: "alert-danger", alert: "alert-warning", notice: "alert-info" }[flash_type.to_sym] || flash_type.to_s
  end

  def flash_messages(opts = {})
    flash.each do |msg_type, message|
      if message.kind_of?(Array)
        message.each do |m|
          concat(render_message(m, msg_type))
        end
      else
        concat(render_message(message, msg_type))
      end

    end
    nil
  end

  def render_message message, msg_type
    content_tag(:div, message, class: "alert #{bootstrap_class_for(msg_type)} fade in") do
            concat content_tag(:button, 'x', class: "close", data: { dismiss: 'alert' })
            concat message
          end
  end
end
