class MoneyController < ApplicationController
  before_action :authenticate_user!
  def new
  end

  def update
    errors = []
    if params["credit_card"] != "4444444444444444"
      errors << "Invalid credit card number!"
    end
    if params["security_code"] != "123"
      errors << "Invalid security code!"
    end
    if params["expiry_year"] != "2020"
      errors << "Invalid expiry year number!"
    end
    if params["expiry_month"] != "01"
      errors << "Invalid expiry month number!"
    end
    amount = BigDecimal(params["amount"])
    if amount <= 0
      errors << "Invalid amount!"
    end
    if errors.length > 0
      flash.now[:error] = errors
      render action: :new
    else
      current_user.update_attribute(:money, current_user.money += amount)
      redirect_to money_add_path, flash: {success: 'Money added successfully!'}
    end


  end
end
