class PromotionController < ApplicationController

  before_action :authenticate_user!

  def claim
  end

  def apply
    p = Promotion.find(params[:promotion_id])
    p.user.update_attribute(:money, p.user.money + p.amount)
    p.update_attribute(:applied, true)
    redirect_to promotion_claim_path, flash: {success: 'Promotion applied successfully!'}
  end
end
